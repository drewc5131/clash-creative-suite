import hashlib
import os
jsonString = '{\n"data": ['
rootDir = 'D:\Users\Drew\Documents\clash-creative-suite'
for dirName, subdirList, fileList in os.walk(rootDir):
    print('Found directory: %s' % dirName)
    for fname in fileList:
        if not dirName.startswith("D:\\Users\\Drew\\Documents\\clash-creative-suite\\.git"):
            filename = "%s\\%s"%(dirName.replace("D:\\Users\\Drew\\Documents\\clash-creative-suite\\" ,""), fname)
            jsonString = jsonString + '{"Filename": "%s", "sha256" : "%s"},\n' %(filename, os.stat("%s" %"%s\\%s" %(dirName, fname)).st_size)
    
jsonString = jsonString[:-2] + '\n]}'
jsonString = jsonString.replace("\\", "/").replace("D:/Users/Drew/Documents/clash-creative-suite/", "")
print(jsonString)
file = open("ccs.json", "w")
file.write(jsonString)